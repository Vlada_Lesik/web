var submitButton = document.getElementById('submit_button');
var title;
var theme;
var email;
var bigText;
var tags;
function checkParams() {
    title = document.getElementById('title').value;
    theme = document.getElementById('short_theme').value;
    email = document.getElementById('email_field').value;
    bigText = document.getElementById('big_text').value;
    tags = document.getElementById('tags').value;

     if(title.length !== 0 && email.length !== 0 && theme.length !== 0 && bigText.length !== 0 && tags.length !== 0) {
        submitButton.removeAttribute('disabled')
     } else {
        submitButton.attributes('disabled', 'disabled');
    }
}

function onSubmitClick(){
    alert(`Your request: ${title}, ${bigText}, ${tags}, ${theme}, ${email}`)
    console.log(`${title}, ${bigText}, ${tags}, ${theme}, ${email}`)
}
submitButton.addEventListener("click", onSubmitClick);